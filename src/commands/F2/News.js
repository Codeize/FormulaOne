const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');

class News extends patron.Command {
    constructor() {
        super({
            names: ['news'],
            groupName: 'f2',
            description: 'Send a news link to #news.',
            args: [
                new patron.Argument({
                    name: 'link',
                    key: 'link',
                    type: 'string',
                    example: 'https://www.bbc.com/sport/formula1/44930765'
                })
            ]
        });
    }

    async run(msg, args) {
        const blacklistedNews = Constants.blacklistedNews;
        const blockedNews = Constants.blockedNews;
        const newsChannel = await msg.guild.channels.resolve('335167453350854666');
        const pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        if (!pattern.test(args.link)) {
            return msg.sender.reply('That is not a valid URL.', { color: Constants.errorColor });
        }
        if (args.link.includes('youtube')) {
            return msg.sender.reply('You are not allowed to post YouTube links.', { color: Constants.errorColor });
        }
        for (let i = 0; i < blacklistedNews.length; i++) {
            if (args.link.includes(blacklistedNews[i])) {
                await msg.delete();
                await msg.member.roles.add(msg.dbGuild.roles.muted);
                await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Automatic Mute', Constants.kickColor, 'Attempted to post a blacklisted link to ' + newsChannel.toString(), null, msg.author, 'Link', args.link);
                return msg.sender.dm(msg.author, 'You have been automatically muted for attempting to post a blacklisted link into ' + newsChannel.toString() + ', if you believe this is an error please contact a Moderator.');
            }
        }

        for (let i = 0; i < blockedNews.length; i++) {
            if (args.link.includes(blockedNews[i])) {
                await msg.delete();
                return msg.sender.reply('That news source is currently blacklisted.', { color: Constants.errorColor });
            }
        }
        await newsChannel.send(`${args.link} sent by ${msg.author.tag}`);
        return msg.sender.reply('Successfully posted to ' + newsChannel.toString());
    }
}

module.exports = new News();
