const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const db = require('../../database');

class AddAssignableRole extends patron.Command {
    constructor() {
        super({
            names: ['addrole', 'addrank', 'addassignablerole', 'addassignrole'],
            groupName: 'administrator',
            description: 'Add an assignable role to the list.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Ferrari',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        if (msg.dbGuild.roles.assignable.some((role) => role.id === args.role.id) === true) {
            return msg.sender.reply('The ' + args.role.toString() + ' role is already assignable.', { color: Constants.errorColor });
        }

        await db.guildRepo.upsertGuild(msg.guild.id, new db.updates.Push('roles.assignable', { id: args.role.id }));
        return msg.sender.reply('Successfully made the ' + args.role.toString() + ' role assignable.');
    }
}

module.exports = new AddAssignableRole();
