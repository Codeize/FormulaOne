const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class AssignRole extends patron.Command {
    constructor() {
        super({
            names: ['rank', 'team', 'role', 'assignrole', 'unassignrole'],
            groupName: 'administrator',
            description: 'Assign or unassign an assignable role.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Ferrari',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const roles = msg.dbGuild.roles.assignable;

        for (let i = 0; i < roles.length; i++) {
            if (args.role.id === roles[i].id) {
                if (msg.member.roles.cache.has(args.role.id)) {
                    await msg.member.roles.remove(args.role);
                    return msg.sender.reply('Successfully unassigned role ' + args.role.toString());
                }
                await msg.member.roles.add(args.role);
                return msg.sender.reply('Successfully assigned role ' + args.role.toString());
            }
        }
        return msg.sender.reply(args.role.toString() + ' is not assignable.', { color: Constants.errorColor });
    }
}

module.exports = new AssignRole();
