const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class AssignableRoles extends patron.Command {
    constructor() {
        super({
            names: ['roles', 'ranks', 'assignableroles'],
            groupName: 'administrator',
            description: 'View all of the assignable roles.'
        });
    }

    async run(msg, args) {
        if (msg.dbGuild.roles.assignable.length === 0) {
            return msg.sender.reply('No assignable roles found.', { color: Constants.errorColor });
        }

        const roles = msg.dbGuild.roles.assignable;

        let description = '';

        for (let i = 0; i < roles.length; i++) {
            const rank = await msg.guild.roles.fetch(roles[i].id);

            description += rank.toString() + '\n';
        }

        return msg.sender.send(description, { title: 'Assignable Roles' });
    }
}

module.exports = new AssignableRoles();
