const patron = require('patron.js');
const StringUtil = require('../../utility/StringUtil.js');
const ModerationService = require('../../services/ModerationService.js');
const Constants = require('../../utility/Constants.js');
const db = require('../../database/index.js');

class Ban extends patron.Command {
    constructor() {
        super({
            names: ['ban'],
            groupName: 'administrator',
            description: 'Ban a user.',
            args: [
                new patron.Argument({
                    name: 'user',
                    key: 'user',
                    type: 'user',
                    example: 'coco-bun#6681',
                    preconditions: ['nomoderator']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Posting an IP logger.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        console.log("Test");
        const date = new Date();
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        if (msg.guild.members.cache.has(args.user.id)) {
            msg.sender.dm(args.user, 'A moderator has banned you' + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason + '.'));
            await db.userRepo.upsertUser(args.user.id, msg.guild.id, { $inc: { bans: 1 } });
        }
        await msg.guild.members.ban(args.user);
        await msg.sender.reply('Successfully banned ' + StringUtil.boldify(args.user.tag) + '.');
        await db.userRepo.upsertUser(args.user.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: 'Ban', reason: args.reason, mod: msg.author.tag, channelId: msg.channel.id }));
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Ban', Constants.banColor, args.reason, msg.author, args.user);
    }
}

module.exports = new Ban();
