const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const StringUtil = require('../../utility/StringUtil.js');

class Jezza extends patron.Command {
    constructor() {
        super({
            names: ['jezza'],
            groupName: 'administrator',
            description: 'Applies light mode to a user.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'Jizza#0005',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await args.member.roles.remove([Constants.teamRoles.ferrari, Constants.teamRoles.mercedes, Constants.teamRoles.redbull, Constants.teamRoles.renault,
            Constants.teamRoles.haas, Constants.teamRoles.racingpoint, Constants.teamRoles.mclaren, Constants.teamRoles.alphatauri, Constants.teamRoles.alfaromeo,
            Constants.teamRoles.williams]);
        await args.member.roles.add([Constants.teamRoles.ferrari, Constants.teamRoles.mercedes, Constants.teamRoles.redbull, Constants.teamRoles.renault,
            Constants.teamRoles.haas, Constants.teamRoles.racingpoint, Constants.teamRoles.mclaren, Constants.teamRoles.alphatauri, Constants.teamRoles.alfaromeo,
            Constants.teamRoles.williams]);
        return msg.sender.reply('Successfully applied all team roles to ' + StringUtil.boldify(args.member.user.tag) + '.');
    }
}

module.exports = new Jezza();
