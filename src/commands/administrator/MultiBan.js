const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const client = require('../../singletons/client.js');

class Multiban extends patron.Command {
    constructor() {
        super({
            names: ['multiban', 'mban'],
            groupName: 'administrator',
            description: 'Ban multiple ids at once, separate them with spaces.',
            args: [
                new patron.Argument({
                    name: 'ids',
                    key: 'ids',
                    type: 'string',
                    example: '475013414566232094 467574105722322944 467573667249913878',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const users = args.ids.split(" ");
        let message = '';
        let successfulBans = '';
        for (let i = 0; i < users.length; i++) {
            const removedW = users[i].replace(/\s/g, '').toString();
            const specificMember = await msg.guild.members.fetch(removedW);
            const specificUser = await client.users.fetch(removedW);
            if (specificUser === null || specificUser === undefined) {
                message+= `Could not find user from ID ${removedW}\n`;
                continue;
            }
            if (specificMember !== undefined && specificMember !== null) {
                if (ModerationService.getPermLevel(msg.dbGuild, specificMember) >= 1) {
                    message+= `Could not ban ${removedW} (${specificMember.user.tag}) as they are a Moderator.`;
                    continue;
                }
            }
            await msg.guild.members.ban(specificUser);
            successfulBans+= `${removedW} (${specificUser.tag})`;
            message+= `Banned ${removedW} (${specificUser.tag})\n`;
        }
        if (successfulBans !== '') {
            await ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, `Banned multiple IDs: \n${successfulBans}`);
        }
        return msg.sender.send(message, { title: 'Multiban Results' });
    }
}

module.exports = new Multiban();
