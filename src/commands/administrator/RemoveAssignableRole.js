const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const db = require('../../database');

class RemoveAssignableRole extends patron.Command {
    constructor() {
        super({
            names: ['removerole', 'removerank', 'removeassignablerole', 'removeassignrole'],
            groupName: 'administrator',
            description: 'Remove an assignable role to the list.',
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Ferrari',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        if (msg.dbGuild.roles.assignable.some((role) => role.id === args.role.id) === false) {
            return msg.sender.reply('The ' + args.role.toString() + ' role is not assignable.', { color: Constants.errorColor });
        }

        await db.guildRepo.upsertGuild(msg.guild.id, new db.updates.Pull('roles.assignable', { id: args.role.id }));
        return msg.sender.reply('Successfully removed the assignable role for ' + args.role.toString() + '.');
    }
}

module.exports = new RemoveAssignableRole();
