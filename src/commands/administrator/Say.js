const patron = require('patron.js');
const Sender = require("../../utility/Sender");

class Say extends patron.Command {
    constructor() {
        super({
            names: ['say', 'sendmsg', 'sendmessage'],
            groupName: 'administrator',
            description: 'Send a message to a channel.',
            args: [
                new patron.Argument({
                    name: 'channel',
                    key: 'channel',
                    type: 'textchannel',
                    example: 'f1-banter',
                }),
                new patron.Argument({
                    name: 'message',
                    key: 'message',
                    type: 'string',
                    example: 'Hello this is a message.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await Sender.send(args.channel, args.message);
        return msg.sender.reply('Successfully sent the message.');
    }
}

module.exports = new Say();
