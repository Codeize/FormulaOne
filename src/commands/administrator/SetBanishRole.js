const patron = require('patron.js');
const db = require('../../database');

class SetBanishRole extends patron.Command {
    constructor() {
        super({
            names: ['setbanishedrole', 'setbanishrole', 'setbanish', 'setbanished'],
            groupName: 'administrator',
            description: 'Sets the banish role.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Banished',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'roles.banished': args.role.id } });
        return msg.sender.reply('Successfully set the banished role to ' + args.role.toString() + '.');
    }
}

module.exports = new SetBanishRole();
