const patron = require('patron.js');
const db = require('../../database');

class SetMutedRole extends patron.Command {
    constructor() {
        super({
            names: ['setmutedrole', 'setmuterole', 'setmute', 'setmuted'],
            groupName: 'administrator',
            description: 'Sets the muted role.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Muted',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'roles.muted': args.role.id } });
        return msg.sender.reply('Successfully set the muted role to ' + args.role.toString() + '.');
    }
}

module.exports = new SetMutedRole();
