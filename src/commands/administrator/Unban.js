const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class Unban extends patron.Command {
    constructor() {
        super({
            names: ['unban'],
            groupName: 'administrator',
            description: 'Unban a user.',
            args: [
                new patron.Argument({
                    name: 'user',
                    key: 'user',
                    type: 'banneduser',
                    example: 'Fredrick#4872'
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Forgiven <3.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await msg.guild.members.unban(args.user);
        await msg.sender.reply('Successfully unbanned ' + StringUtil.boldify(args.user.tag) + '.');
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Unban', Constants.unbanColor, args.reason, msg.author, args.user);
        return msg.sender.dm(args.user, 'A moderator has unbanned you' + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason + '.'));
    }
}

module.exports = new Unban();
