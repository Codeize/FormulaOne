const patron = require('patron.js');
const db = require('../../database');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class Blacklist extends patron.Command {
    constructor() {
        super({
            names: ['bl', 'blacklist'],
            groupName: 'botowners',
            description: 'Blacklist any user from using Formula One.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'User#4925',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
        if (dbUser.blacklisted) {
            await msg.sender.reply('Successfully un-blacklisted ' + StringUtil.boldify(args.member.user.tag));
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $set: { blacklisted: false } });
            return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Bot Un-Blacklist', Constants.banColor, null, msg.author, args.member.user);
        } else {
            await msg.sender.reply('Successfully blacklisted ' + StringUtil.boldify(args.member.user.tag));
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $set: { blacklisted: true } });
            return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Bot Blacklist', Constants.banColor, null, msg.author, args.member.user);
        }
    }
}

module.exports = new Blacklist();
