const patron = require('patron.js');
const db = require('../../database');
const util = require('util');
const Constants = require('../../utility/Constants.js');
const Discord = require('discord.js');

class Eval extends patron.Command {
    constructor() {
        super({
            names: ['eval'],
            groupName: 'botowners',
            description: 'Evalute JavaScript code.',
            guildOnly: false,
            args: [
                new patron.Argument({
                    name: 'code',
                    key: 'code',
                    type: 'string',
                    example: 'client.token',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        try {
            const client = msg.client;
            const message = msg;
            const guild = msg.guild;
            const dbGuild = msg.dbGuild;
            const channel = msg.channel;
            const author = msg.author;
            const member = msg.member;
            const database = db;

            let result = eval(args.code);

            if (result.then !== undefined) {
                result = await result;
            }

            if (typeof result !== 'string') {
                result = util.inspect(result, { depth: 0 });
            }

            result = result.replace(client.token, '[Redacted]').replace(/\[Object\]/g, 'Object').replace(/\[Array\]/g, 'Array');

            return msg.sender.sendFields(['Eval', '```js\n' + args.code + '```', 'Returns', '```js\n' + result + '```']);
        } catch (err) {
            return msg.sender.sendFields(['Eval', '```js\n' + args.code + '```', 'Error', '```js\n' + err + '```'], { color: Constants.errorColor });
        }
    }
}

module.exports = new Eval();
