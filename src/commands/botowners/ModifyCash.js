const patron = require('patron.js');
const db = require('../../database');

class ModifyCash extends patron.Command {
    constructor() {
        super({
            names: ['modifycash', 'modifymoney'],
            groupName: 'botowners',
            description: 'Give a specific user a set amount of money.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'coco-bun#6681'
                }),
                new patron.Argument({
                    name: 'amount',
                    key: 'amount',
                    type: 'int',
                    example: '400000'
                })
            ]
        });
    }

    async run(msg, args) {
        await db.userRepo.modifyBalance(msg.dbGuild, args.member, args.amount);
        return msg.sender.reply('Successfully modified ' + args.member.user.tag + '\'s balance.');
    }
}

module.exports = new ModifyCash();
