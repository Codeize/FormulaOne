const db = require('../../database/index.js');
const patron = require('patron.js');

class ResetPuns extends patron.Command {
    constructor() {
        super({
            names: ['resetpuns'],
            groupName: 'botowners',
            description: 'Reset all punishment data.',
        });
    }

    async run(msg, args) {
        await msg.sender.reply('Are you sure you wish to reset all F1 punishment related data within your server? Reply with "yes" to continue.');

        const filter = (x) => x.content.toLowerCase() === 'yes' && x.author.id === msg.author.id;
        const result = await msg.channel.awaitMessages(filter, { max: 1, time: 30000 });

        if (result.size === 1) {
            await db.punRepo.deleteMany();
            return msg.sender.reply('Successfully reset all F1 punishment data in your guild.');
        }
    }
}

module.exports = new ResetPuns();
