const db = require('../../database/index.js');
const patron = require('patron.js');

class ResetUser extends patron.Command {
    constructor() {
        super({
            names: ['resetusers'],
            groupName: 'botowners',
            description: 'Reset all user data.',
        });
    }

    async run(msg, args) {
        await msg.sender.reply('Are you sure you wish to reset all F1 user related data within your server? Reply with "yes" to continue.');

        const filter = (x) => x.content.toLowerCase() === 'yes' && x.author.id === msg.author.id;
        const result = await msg.channel.awaitMessages(filter, { max: 1, time: 30000 });

        if (result.size === 1) {
            await db.userRepo.deleteUsers(msg.guild.id);
            return msg.sender.reply('Successfully reset all F1 user data in your guild.');
        }
    }
}

module.exports = new ResetUser();
