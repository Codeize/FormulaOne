const patron = require('patron.js');
const client = require('../../singletons/client.js');

class Status extends patron.Command {
    constructor() {
        super({
            names: ['status', 'playing'],
            groupName: 'botowners',
            description: 'Set the bot status.',
            args: [
                new patron.Argument({
                    name: 'type',
                    key: 'type',
                    type: 'string',
                    preconditions: ['statustype'],
                    example: 'PLAYING'
                }),
                new patron.Argument({
                    name: 'status',
                    key: 'status',
                    type: 'string',
                    example: '$help',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await client.user.setActivity(args.status, { type });
        return msg.sender.reply(`Successfully set ${type} to ` + args.status);
    }
}

module.exports = new Status();
