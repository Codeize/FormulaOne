const patron = require('patron.js');

class UnmuteSelf extends patron.Command {
    constructor() {
        super({
            names: ['unmuteself'],
            groupName: 'botowners',
            description: 'Unmute yourself.'
        });
    }

    async run(msg, args) {
        await msg.sender.reply('Successfully unmuted yourself.');
        return msg.member.roles.remove(msg.dbGuild.roles.muted);
    }
}

module.exports = new UnmuteSelf();
