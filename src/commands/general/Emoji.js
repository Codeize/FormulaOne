const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const Sender = require('../../utility/Sender.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class Emoji extends patron.Command {
    constructor() {
        super({
            names: ['emoji', 'emote'],
            groupName: 'general',
            description: 'Send an emoji submission to #emojis.',
            args: [
                new patron.Argument({
                    name: 'name',
                    key: 'name',
                    type: 'string',
                    example: 'superhard'
                }),
                new patron.Argument({
                    name: 'url',
                    key: 'url',
                    type: 'string',
                    example: 'https://cdn.discordapp.com/attachments/424590803936083969/639541326970617876/SEBASTIAN_VETTEL__99207.png',
                    defaultValue: ''
                })
            ]
        });
    }

    async run(msg, args) {
        if (msg.attachments.size === 0 && StringUtil.isNullOrWhiteSpace(args.url)) {
            return msg.sender.reply('You must upload an image or specify a URL after the emoji name.', { color: Constants.errorColor });
        }
        let imageUrl;
        if (msg.attachments.size >= 1) {
            imageUrl = msg.attachments.first().url;
        } else {
            imageUrl = args.url;
        }
        if (!imageUrl.includes(".png") && !imageUrl.includes(".jpg") && !imageUrl.includes(".jpeg") && !imageUrl.includes(".gif")) {
            return msg.sender.reply('That is not a valid file type, please make sure you include the direct image/gif URL.', { color: Constants.errorColor });
        }
        const emojiChannel = await msg.guild.channels.resolve('639401538485485569');
        for (let i = 0; i < Constants.blacklistedNews.length; i++) {
            if (args.name.includes(Constants.blacklistedNews[i])) {
                await msg.delete();
                await msg.member.roles.add(msg.dbGuild.roles.muted);
                await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Automatic Mute', Constants.kickColor, 'Attempted to request a blacklisted name to ' + emojiChannel, null, msg.author, 'Emoji Name', args.name);
                return msg.sender.dm(msg.author, 'You have been automatically muted for attempting to post a blacklisted name into ' + emojiChannel.toString() + ', if you believe this is an error please contact a Moderator.');
            }
        }
        const options = {
            timestamp: true
        };

        options.author = {
            name: msg.author.tag,
            icon_url: msg.author.displayAvatarURL()
        };

        options.image = {
            url: imageUrl
        };
        const sent = await Sender.send(emojiChannel, 'Proposed the emote :' + args.name + ':', options);
        await sent.react('303230406352699393');
        await sent.react('303230394008993793');
        await msg.sender.reply('Successfully proposed the emoji :' + args.name + ':.');
    }
}

module.exports = new Emoji();
