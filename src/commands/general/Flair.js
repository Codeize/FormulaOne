const patron = require('patron.js');

class Flair extends patron.Command {
    constructor() {
        super({
            names: ['flair', 'setflair', 'delflair', 'resetflair'],
            groupName: 'general',
            description: 'Change your flair.',
            args: [
                new patron.Argument({
                    name: 'flair',
                    key: 'flair',
                    type: 'string',
                    defaultValue: '',
                    example: 'LEC',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        if (args.flair === '') {
            await msg.member.setNickname(msg.author.username, 'Bot Flair Reset');
            return msg.sender.reply('Successfully removed your flair.');
        }
        await msg.member.setNickname(`${msg.author.username} [${args.flair}]`, 'Bot Flair Change.');
        return msg.sender.reply('Successfully set your flair to ' + args.flair + '.');
    }
}

module.exports = new Flair();
