const patron = require('patron.js');
const StringUtil = require('../../utility/StringUtil.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');

class Help extends patron.Command {
  constructor() {
    super({
      names: ['help', 'commands', 'cmds', 'cmd'],
      groupName: 'general',
      description: 'View all of the command information.',
      args: [
        new patron.Argument({
          name: 'command',
          key: 'command',
          type: 'string',
          defaultValue: patron.ArgumentDefault.Member,
          example: 'commands',
          remainder: true
        })
      ]
    });
  }

  async run(msg, args) {
    if (StringUtil.isNullOrWhiteSpace(args.command)) {
      const permLevel = ModerationService.getPermLevel(msg.dbGuild, msg.member);
      const commands = msg.client.registry.commands;
      let generalCommands = [];
      let marshalCommands = [];
      let stewardCommands = [];
      let ownerCommands = [];
      for (let i = 0; i < commands.length; i++) {
        switch (commands[i].group.name) {
          case 'general':
            generalCommands.push({"name": '$' + commands[i].names[0], "description": commands[i].description});
            break;
          case 'moderation':
            marshalCommands.push({ "name": '$' + commands[i].names[0], "description": commands[i].description });
            break;
          case 'administrator':
            stewardCommands.push({ "name": '$' + commands[i].names[0], "description": commands[i].description });
            break;
          case 'owner':
            ownerCommands.push({ "name": '$' + commands[i].names[0], "description": commands[i].description });
            break;
          case 'f2':
            generalCommands.push({ "name": '$' + commands[i].names[0], "description": 'Requires F2 Role - ' + commands[i].description });
            break;
          case 'f4':
            generalCommands.push({ "name": '$' + commands[i].names[0], "description": 'Requires F4 Role - ' + commands[i].description });
            break;
        }
      }
      let message = '**General**\n';
      let messageMarsh = '**Marshals**\n';
      let message2 = '**Stewards**\n';
      for (let i = 0; i < generalCommands.length; i++) {
          message += '`' + generalCommands[i].name + '` - ' + generalCommands[i].description + '\n';
      }
      if (permLevel >= 1) {
        for (let i = 0; i < marshalCommands.length; i++) {
          messageMarsh += '`' + marshalCommands[i].name + '` - ' + marshalCommands[i].description + '\n';
        }
      }

      if (permLevel >= 2) {
        for (let i = 0; i < stewardCommands.length; i++) {
          message2 += '`' + stewardCommands[i].name + '` - ' + stewardCommands[i].description + '\n';
        }
      }

      if (permLevel === 3) {
        message2 += '\n**Server Admins**\n';
        for (let i = 0; i < ownerCommands.length; i++) {
          message2 += '`' + ownerCommands[i].name + '` - ' + ownerCommands[i].description + '\n';
        }
      }

      if (message2 === '**Stewards**\n') {
        message += '\nYou can find a specific command\'s information by using `$help [Command]`\n\n**Formula One** is made by **Fozzie#0001**.';
      }
      await msg.sender.dm(msg.author, message, { 'title': 'Commands' });

      if (messageMarsh !== '**Marshals**\n') {
        if (message2 === '**Stewards**\n') {
          messageMarsh += '\nYou can find a specific command\'s information by using `$help [Command]`\n\n**Formula One** is made by **Fozzie#0001**.';
        }
        await msg.sender.dm(msg.author, messageMarsh);
      }
      if (message2 !== '**Stewards**\n') {
        message2 += '\nYou can find a specific command\'s information by using `$help [Command]`\n\n**Formula One** is made by **Fozzie#0001**.';
        await msg.sender.dm(msg.author, message2);
      }
      return msg.sender.reply('Successfully DMed you all command information.');
    } else {
      args.command = args.command.startsWith(Constants.prefix) ? args.command.slice(Constants.prefix.length) : args.command;

      const lowerInput = args.command.toLowerCase();

      const command = msg.client.registry.commands.find((x) => x.names.some((y) => y === lowerInput));

      if (command === undefined) {
        return msg.sender.reply('This command does not exist.', { color: Constants.errorColor });
      }

      return msg.sender.send('**Description:** ' + command.description +  '\n**Usage:** `' + Constants.prefix + command.getUsage() + '`\n**Example:** `' + Constants.prefix + command.getExample() + '`', { title: StringUtil.upperFirstChar(command.names[0]) });
    }
  }
}

module.exports = new Help();
