const patron = require('patron.js');
const Discord = require('discord.js');
const Constants = require('../../utility/Constants.js');
const Random = require('../../utility/Random.js');

class Info extends patron.Command {
  constructor() {
    super({
      names: ['info', 'members', 'guildinfo', 'serverinfo', 'server'],
      groupName: 'general',
      description: 'View all of the Guild information.'
    });
  }

  async run(msg, args) {
    if (!msg.guild.available) {
      return msg.sender.reply('The Guild is currently busy, please try again later.');
    }
    const embed = new discord.MessageEmbed()
      .setColor(Random.arrayElement(Constants.defaultColors))
      .setTitle(msg.guild.name)
      .setThumbnail(msg.guild.iconURL)
      .addField('Owner', msg.guild.owner.user.tag)
      .addField('Guild ID', msg.guild.id)
      .addField('Region', msg.guild.region)
      .addField('Members', msg.guild.memberCount);

    return msg.channel.send({ embed });
  }
}

module.exports = new Info();
