const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');

class ModRoles extends patron.Command {
  constructor() {
    super({
      names: ['modroles', 'modrole', 'mr', 'mrs'],
      groupName: 'general',
      description: 'View all moderation roles in this guild.'
    });
  }

  async run(msg, args) {
    const modRoleList = msg.dbGuild.roles.mod.sort((a, b) => a.permissionLevel - b.permissionLevel);

    if (msg.dbGuild.roles.mod.length === 0) {
      return msg.sender.reply('No moderation role data found.', { color: Constants.errorColor });
    }

    let description = '';
    for (let i = 0; i < modRoleList.length; i++) {
      const rank = msg.guild.roles.find((x) => x.id === modRoleList[i].id);

      description += rank + ': ' + modRoleList[i].permissionLevel + '\n';
    }

    return msg.sender.send(description + '\n**Permission Levels:**\n**1)** Marshal\n**2)** Steward\n**3)** Server Admin\n\nYour permission level: ' + ModerationService.getPermLevelStr(msg.dbGuild, msg.member), { title: 'Moderation Roles' });
  }
}

module.exports = new ModRoles();
