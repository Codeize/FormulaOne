const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const Try = require('../../utility/Try.js');
const ModerationService = require('../../services/ModerationService.js');

class Report extends patron.Command {
    constructor() {
        super({
            names: ['report'],
            groupName: 'general',
            description: 'Send a report to the Moderators.',
            args: [
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Racism arguments.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await msg.delete();
        Try(msg.sender.dm(msg.author, 'Successfully reported for ' + args.reason));
        return ModerationService.tryReportLog(msg.guild, args.reason, msg.author, msg.channel, Constants.kickColor);
    }
}

module.exports = new Report();
