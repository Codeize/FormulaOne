const patron = require('patron.js');
const NumberUtil = require('../../utility/NumberUtil.js');

class Stats extends patron.Command {
    constructor() {
        super({
            names: ['stats', 'statistics', 'stat'],
            groupName: 'general',
            description: 'View the Bot\'s statistics.',
        });
    }

    async run(msg, args) {
        const uptime = NumberUtil.msToTime(msg.client.uptime);
        return msg.sender.sendFields([
            'Author', 'Fozzie#0001',
            'Memory', (process.memoryUsage().rss / 1048576).toFixed(2) + 'MB',
            'Node.js version', 'v' + process.versions.node,
            'Uptime', 'Days: ' + uptime.days + '\nHours: ' + uptime.hours + '\nMinutes: ' + uptime.minutes
        ]);
    }
}

module.exports = new Stats();
