const patron = require('patron.js');
const BanishUtil  = require('../../utility/BanishUtil.js');

class Banish extends patron.Command {
    constructor() {
        super({
            names: ['banish', 'banished'],
            groupName: 'moderation',
            description: 'Banish a member.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        return BanishUtil.banishMember(msg, args);
    }
}

module.exports = new Banish();
