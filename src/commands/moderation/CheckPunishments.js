const db = require('../../database/index');
const patron = require('patron.js');
const Punishment = require('../../utility/Punishment.js');
const StringUtil = require('../../utility/StringUtil.js');
const Constants = require('../../utility/Constants.js');

class CheckPunishments extends patron.Command {
    constructor() {
        super({
            names: ['checkpunishments', 'checkpunishment', 'checkpuns', 'checkpun', 'checkpunish'],
            groupName: 'moderation',
            description: 'Check the number of punishments a user has in the last 30 days.',
            args: [
                new patron.Argument({
                    name: 'user',
                    key: 'user',
                    type: 'user',
                    example: 'John#5974',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const dbUser = await db.userRepo.getUser(args.user.id, msg.guild.id);
        const currentPun = await dbUser.currentPunishment;
        const allPuns = await dbUser.punishments;
        if (allPuns.length === 0) {
            return msg.sender.send(StringUtil.boldify(args.user.tag) + ' has a clean slate.', { color: Constants.greenColor });
        }
        await allPuns.sort((a, b) => b.date - a.date);
        let fields = [];
        for (let i = 0; i < allPuns.length; i++) {
            const specificPun = allPuns[i];
            fields.push(specificPun.readableDate);
            fields.push('**Escalation:** ' + specificPun.escalation + '\n**Moderator:** ' + specificPun.mod + (specificPun.reason === '' ? '' : '\n**Reason:** ' + specificPun.reason) + '\n**Channel:** ' + await msg.guild.channels.resolve(specificPun.channelId).toString() + '\n\n');
        }

        const options = {
            title: args.user.tag + '\'s Punishment History'
        };
        options.footer = {
            text: args.user.tag + ' has ' + currentPun + ' punishments in the last 30 days.'
        };

        return msg.sender.sendFields(fields, options);
    }
}

module.exports = new CheckPunishments();
