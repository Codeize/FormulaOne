const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class DM extends patron.Command {
    constructor() {
        super({
            names: ['dm', 'msg', 'pm', 'message'],
            groupName: 'moderation',
            description: 'DM a member a message.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['noself']
                }),
                new patron.Argument({
                    name: 'message',
                    key: 'message',
                    type: 'string',
                    example: 'Please stop saying that, this won\'t count as a punishment.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await msg.sender.reply('Successfully DMed ' + StringUtil.boldify(args.member.user.tag) + '.')
        return msg.sender.dm(args.message, { title: 'A message from a Moderator' }, args.member.user);
    }
}

module.exports = new DM();
