const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class Delete extends patron.Command {
    constructor() {
        super({
            names: ['delete'],
            groupName: 'moderation',
            description: 'Delete a message from a channel by ID.',
            args: [
                new patron.Argument({
                    name: 'channel',
                    key: 'channel',
                    type: 'textchannel',
                    example: '#formula1'
                }),
                new patron.Argument({
                    name: 'message id',
                    key: 'messageId',
                    type: 'string',
                    example: '490420346764394518',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const mess = await args.channel.messages.fetch(args.messageId);
        if (mess === undefined || mess === null) {
            return msg.sender.reply('Cannot find message ' + args.messageId, { color: Constants.errorColor });
        }
        await mess.delete();
        return msg.sender.reply('Successfully deleted message ' + args.messageId);
    }
}

module.exports = new Delete();
