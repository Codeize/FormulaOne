const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const StringUtil = require('../../utility/StringUtil.js');

class GiveRole extends patron.Command {
    constructor() {
        super({
            names: ['giverole'],
            groupName: 'moderation',
            description: 'Give a user a role.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'Fozzie#0001'
                }),
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Member',
                    preconditions: ['hierarchyuser', 'hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const mutedRole = msg.dbGuild.roles.muted;
        if (args.role.id === mutedRole) {
            return msg.sender.reply('You cannot manually assign the Muted role. Please use $mute.', { color: Constants.errorColor });
        }
        await args.member.roles.add(args.role);
        return msg.sender.reply('Successfully given ' + StringUtil.boldify(args.member.user.tag) + ' the ' + args.role.toString() + ' role.');
    }
}

module.exports = new GiveRole();
