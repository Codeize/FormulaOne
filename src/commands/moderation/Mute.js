const patron = require('patron.js');
const db = require('../../database');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');
const NumberUtil = require('../../utility/NumberUtil.js');
const Try = require('../../utility/Try.js');

class Mute extends patron.Command {
    constructor() {
        super({
            names: ['mute', 'silence'],
            groupName: 'moderation',
            description: 'Mute any user for a specified time, default is minutes.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'Badboy#4925',
                    preconditions: ['nomoderator']
                }),
                new patron.Argument({
                    name: 'time',
                    key: 'time',
                    type: 'string',
                    example: '10m'
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Breaking rules.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const role = await msg.guild.roles.fetch(msg.dbGuild.roles.muted);
        const time = args.time.toLowerCase();
        const timeNum = time.replace(/\D/g, '');
        const date = new Date();
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        let timeMS;
        let timeUnit;
        if (time.includes("h")) {
            timeMS = NumberUtil.hoursToMs(timeNum);
            timeUnit = 'hours';
        } else if (time.includes("m")) {
            timeMS = NumberUtil.minutesToMs(timeNum);
            timeUnit = 'minutes';
        } else if (time.includes("d")) {
            timeMS = NumberUtil.daysToMs(timeNum);
            timeUnit = 'days';
        } else if (!isNaN(time)) {
            timeMS = NumberUtil.minutesToMs(timeNum);
            timeUnit = 'minutes';
        } else {
            return msg.sender.reply('Invalid time format, formats: h (Hours), m (Minutes), d (Days).', { color: Constants.errorColor });
        }

        if (msg.dbGuild.roles.muted === null) {
            return msg.sender.reply('Set a muted role with the `' + Constants.prefix + 'setmute <Role>` command before you can mute users.', { color: Constants.errorColor });
        } else if (args.member.roles.cache.has(msg.dbGuild.roles.muted)) {
            return msg.sender.reply('Member is already muted.', { color: Constants.errorColor });
        }

        if (role === undefined) {
            return msg.sender.reply('The muted role has been deleted. Please set a new one with the `' + Constants.prefix + 'setmute <Role>` command.', { color: Constants.errorColor });
        }

        await args.member.roles.add(role);
        await db.muteRepo.insertMute(args.member.id, msg.guild.id, timeMS);
        await msg.sender.reply('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for ' + timeNum + ' ' + timeUnit + '.');
        Try(msg.sender.dm(args.member.user, 'A moderator has muted you for ' + timeNum + ' ' + timeUnit + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason)));
        await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
        await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: timeNum + timeUnit + ' Mute', reason: args.reason, mod: msg.author.tag, channelId: msg.channel.id }));
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Mute', Constants.muteColor, args.reason, msg.author, args.member.user, 'Length', timeNum + ' ' + timeUnit);
    }
}

module.exports = new Mute();
