const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class NoXP extends patron.Command {
    constructor() {
        super({
            names: ['noxp', 'nolevel'],
            groupName: 'moderation',
            description: 'Give a member the NoXP role.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const role = await msg.guild.roles.fetch('314910227306643457');
        if (args.member.roles.cache.has(role.id)) {
            await args.member.roles.remove(role);
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Removed No-XP', Constants.unmuteColor, args.reason, msg.author, args.member.user);
            await msg.sender.reply('Successfully removed NoXP from ' + StringUtil.boldify(args.member.user.tag));
            return msg.sender.dm(args.member.user, 'Your NoXP role has been removed by a Moderator. You are now able to gain XP.');
        }
        await args.member.roles.add(role);
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Applied No-XP', Constants.banishColor, args.reason, msg.author, args.member.user);
        await msg.sender.reply('Successfully added NoXP to ' + StringUtil.boldify(args.member.user.tag) + '.');
        return msg.sender.dm(args.member.user, 'A moderator has given you the NoXP role. You are now unable to gain XP.');
    }
}

module.exports = new NoXP();
