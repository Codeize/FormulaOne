const patron = require('patron.js');

class Noob extends patron.Command {
    constructor() {
        super({
            names: ['noob', 'noobs'],
            groupName: 'moderation',
            description: 'Send the noob message.'
        });
    }

    async run(msg, args) {
        const rules = await msg.guild.channels.resolve('177387572505346048');
        msg.delete();
        return msg.sender.send('Go to ' + rules + ' and click a reaction, seriously read the rules.');
    }
}

module.exports = new Noob();
