const patron = require('patron.js');
const Punishment = require('../../utility/Punishment.js');

class Punish extends patron.Command {
    constructor() {
        super({
            names: ['punish'],
            groupName: 'moderation',
            description: 'Punish a member, (Use -s after the reason for auto deletion of the punishment message).',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        return Punishment.runPunishCommand(msg, args);
    }
}

module.exports = new Punish();
