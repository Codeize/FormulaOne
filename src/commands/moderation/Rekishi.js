const patron = require('patron.js');
const Punishment = require('../../utility/Punishment.js');
const BanishUtil = require('../../utility/BanishUtil.js');

class Rekishi extends patron.Command {
    constructor() {
        super({
            names: ['punishbanish', 'punbanish', 'rekishi'],
            groupName: 'moderation',
            description: 'Punish and banish a member, (Use -s after the reason for auto deletion of the punishment message).',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        await Punishment.runPunishCommand(msg, args);
        return BanishUtil.banishMember(msg, args);
    }
}

module.exports = new Rekishi();
