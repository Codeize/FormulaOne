const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const Constants = require('../../utility/Constants.js');
const client = require('../../singletons/client.js');

class Slowmode extends patron.Command {
    constructor() {
        super({
            names: ['slowmode', 'sm'],
            groupName: 'moderation',
            description: 'Enable Discord-slowmode.',
            args: [
                new patron.Argument({
                    name: 'seconds',
                    key: 'seconds',
                    type: 'string',
                    example: '5',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        if (args.seconds === 'off' || args.seconds === 'disable' || args.seconds === '0') {
            await client.api.channels(msg.channel.id).patch({
                data: {
                    rate_limit_per_user: 0
                }
            });
            // await msg.channel.edit({ rateLimitPerUser: 0 }, 'Slowmode Disabled by ' + msg.author.tag);
            await msg.sender.reply('Successfully disabled slowmode in ' + msg.channel.toString() + '.');
            return ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Disabled slowmode in ' + msg.channel.toString() + '.');
        } else if (!isNaN(args.seconds)) {
            const seconds = parseInt(args.seconds);
            await client.api.channels(msg.channel.id).patch({
                data: {
                    rate_limit_per_user: seconds
                }
            });
            // await msg.channel.edit({ rateLimitPerUser: seconds }, 'Slowmode enabled by ' + msg.author.tag);
            await msg.sender.reply('Successfully enabled slowmode in ' + msg.channel.toString() + ' for ' + args.seconds + ' seconds.');
            return ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Enabled slowmode in ' + msg.channel.toString() + ' for ' + args.seconds + ' seconds.');
        } else {
            return msg.sender.reply('You must specify a valid number.', { color: Constants.errorColor });
        }
    }
}

module.exports = new Slowmode();
