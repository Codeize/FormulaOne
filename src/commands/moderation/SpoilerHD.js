const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class SpoilerHD extends patron.Command {
    constructor() {
        super({
            names: ['spoilerhd'],
            groupName: 'moderation',
            description: 'Give a member the SpoilerHD role.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const role = await msg.guild.roles.fetch('308245395136446474');
        if (args.member.roles.cache.has(role.id)) {
            await args.member.roles.remove(role);
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Removed SpoilerHD', Constants.unmuteColor, args.reason, msg.author, args.member.user);
            await msg.sender.reply('Successfully removed SpoilerHD from ' + StringUtil.boldify(args.member.user.tag));
            return msg.sender.dm(args.member.user, 'Your SpoilerHD role has been removed by a Moderator. You are now able to talk in #livechat-hd.');
        }
        await args.member.roles.add(role);
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Applied SpoilerHD', Constants.banishColor, args.reason, msg.author, args.member.user);
        await msg.sender.reply('Successfully added SpoilerHD to ' + StringUtil.boldify(args.member.user.tag) + '.');
        return msg.sender.dm(args.member.user, 'A moderator has given you the SpoilerHD role. You are now unable to talk in #livechat-hd.');
    }
}

module.exports = new SpoilerHD();
