const patron = require('patron.js');
const db = require('../../database');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');
const NumberUtil = require('../../utility/NumberUtil.js');
const Try = require('../../utility/Try.js');

class Tempban extends patron.Command {
    constructor() {
        super({
            names: ['tempban'],
            groupName: 'moderation',
            description: 'Tempbans any user for a specified time, default is minutes.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'user',
                    key: 'user',
                    type: 'user',
                    example: 'Badboy#4925',
                    preconditions: ['nomoderator']
                }),
                new patron.Argument({
                    name: 'time',
                    key: 'time',
                    type: 'string',
                    example: '10m'
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Breaking rules.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const time = args.time.toLowerCase();
        const timeNum = time.replace(/\D/g, '');
        const date = new Date();
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        let timeMS;
        let timeUnit;
        if (time.includes("h")) {
            timeMS = NumberUtil.hoursToMs(timeNum);
            timeUnit = 'hours';
        } else if (time.includes("m")) {
            timeMS = NumberUtil.minutesToMs(timeNum);
            timeUnit = 'minutes';
        } else if (time.includes("d")) {
            timeMS = NumberUtil.daysToMs(timeNum);
            timeUnit = 'days';
        } else if (!isNaN(time)) {
            timeMS = NumberUtil.minutesToMs(timeNum);
            timeUnit = 'minutes';
        } else {
            return msg.sender.reply('Invalid time format, formats: h (Hours), m (Minutes), d (Days).', { color: Constants.errorColor });
        }

        await Try(msg.sender.dm(args.user, 'A moderator has banned you for ' + timeNum + ' ' + timeUnit + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason)));
        await msg.guild.members.ban(args.user);
        await db.banRepo.insertBan(args.user.id, msg.guild.id, timeMS);
        await msg.sender.reply('Successfully banned ' + StringUtil.boldify(args.user.tag) + ' for ' + timeNum + ' ' + timeUnit + '.');
        await db.userRepo.upsertUser(args.user.id, msg.guild.id, { $inc: { bans: 1 } });
        await db.userRepo.upsertUser(args.user.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: timeNum + timeUnit + ' Temp-Ban', reason: args.reason, mod: msg.author.tag, channelId: msg.channel.id }));
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Temp-Ban', Constants.banColor, args.reason, msg.author, args.user, 'Length', timeNum + ' ' + timeUnit);
    }
}

module.exports = new Tempban();
