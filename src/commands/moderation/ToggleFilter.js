const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const db = require('../../database/index.js');

class ToggleFilter extends patron.Command {
    constructor() {
        super({
            names: ['onewordfilter', 'filter', '1wordfilter', 'wordfilter'],
            groupName: 'moderation',
            description: 'Toggle the one word filter for this channel.'
        });
    }

    async run(msg, args) {
        const channelId = msg.channel.id;
        if (msg.dbGuild.enabledChannels.some((channel) => channel.id === channelId) === true) {
            await db.guildRepo.upsertGuild(msg.guild.id, new db.updates.Pull('enabledChannels', { id: channelId }));
            await ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Toggled one word filter for ' + msg.channel.toString() + '\n\n**Status**: Disabled.');
            return msg.sender.reply('Successfully disabled one word filter for ' + msg.channel.toString());
        }
        await db.guildRepo.upsertGuild(msg.guild.id, new db.updates.Push('enabledChannels', { id: channelId }));
        await ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Toggled one word filter for ' + msg.channel.toString() + '\n\n**Status**: Enabled.');
        return msg.sender.reply('Successfully enabled one word filter for ' + msg.channel.toString());
    }
}

module.exports = new ToggleFilter();
