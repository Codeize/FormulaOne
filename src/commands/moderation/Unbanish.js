const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');

class UnBanish extends patron.Command {
    constructor() {
        super({
            names: ['unbanish', 'removebanish'],
            groupName: 'moderation',
            description: 'Remove the banishment any member.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const role = await msg.guild.roles.fetch(msg.dbGuild.roles.banished);
        if (!args.member.roles.cache.has(msg.dbGuild.roles.banished)) {
            return msg.sender.reply(StringUtil.boldify(args.member.user.tag) + ' is not banished.', { color: Constants.errorColor });
        }
        if (role === undefined) {
            return msg.sender.reply('The banished role is not set, please get an Administrator (or Fozzie) to set it using $setbanishrole <Role>', { color: Constants.errorColor });
        }
        await args.member.roles.remove(role.id);
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Un-Banish', Constants.banishColor, args.reason, msg.author, args.member.user);
        await msg.sender.reply('Successfully un-banished ' + StringUtil.boldify(args.member.user.tag) + '.');
        return msg.sender.dm(args.member.user, 'A moderator has un-banished you from all live channels.');
    }
}

module.exports = new UnBanish();
