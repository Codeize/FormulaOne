const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const StringUtil = require('../../utility/StringUtil.js');
const db = require('../../database');
const Try = require('../../utility/Try.js');

class Unmute extends patron.Command {
    constructor() {
        super({
            names: ['unmute'],
            groupName: 'moderation',
            description: 'Unmute any member.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'Paul#3091'
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    defaultValue: '',
                    example: 'Forgiven <3.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        if (msg.dbGuild.roles.muted === null) {
            return msg.sender.reply('Set a muted role with the `' + Constants.data.misc.prefix + 'setmute <Role>` command before you can unmute users.', { color: Constants.errorColor });
        } else if (args.member.roles.cache.has(msg.dbGuild.roles.muted) === false) {
            return msg.sender.reply('User is not muted.', { color: Constants.errorColor });
        }

        const role = await msg.guild.roles.fetch(msg.dbGuild.roles.muted);

        if (role === undefined) {
            return msg.sender.reply('Muted role has been deleted. Please set a new one with the `' + Constants.data.misc.prefix + 'setmute <Role>` command.', { color: Constants.errorColor });
        }

        await args.member.roles.remove(role);
        await db.muteRepo.deleteMute(args.member.user.id, msg.guild.id);
        await msg.sender.reply('Successfully unmuted ' + StringUtil.boldify(args.member.user.tag) + '.');
        Try(msg.sender.dm(args.member.user, 'A moderator has unmuted you' + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason + '.')));
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Unmute', Constants.unmuteColor, args.reason, msg.author, args.member.user);
    }
}

module.exports = new Unmute();
