const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');
const db = require('../../database');
const StringUtil = require('../../utility/StringUtil.js');
const Punishment = require('../../utility/Punishment.js');

class Unpunish extends patron.Command {
    constructor() {
        super({
            names: ['unpunish'],
            groupName: 'moderation',
            description: 'Unpunish any member.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'John#5974',
                    preconditions: ['nomoderator', 'noself']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Accidental punish.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args) {
        const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
        const currentPun = await dbUser.currentPunishment;
        const role = await msg.guild.roles.fetch(msg.dbGuild.roles.muted);
        if (args.member.roles.cache.has(msg.dbGuild.roles.muted)) {
            await args.member.roles.remove(role);
            await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
        }
        if (currentPun <= 0) {
            return msg.sender.reply('User already has less than 1 punishment.', { color: Constants.errorColor })
        }
        await Punishment.decreasePunishment(args.member.id, msg.guild.id);
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Removed last punishment', Constants.muteColor, args.reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun - 1);
        await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Pop('punishments', 1));
        return msg.sender.reply('Successfully removed the last punishment of ' + StringUtil.boldify(args.member.user.tag) + '.');
    }
}

module.exports = new Unpunish();
