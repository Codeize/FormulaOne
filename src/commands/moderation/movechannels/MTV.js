const patron = require('patron.js');
const Constants = require('../../../utility/Constants.js');
const Sender = require('../../../utility/Sender.js');

class MTV extends patron.Command {
    constructor() {
        super({
            names: ['mtv', 'musictv'],
            groupName: 'moderation',
            description: 'Send the move to #music-tv-movies message to a channel, f1-general by default.',
            args: [
                new patron.Argument({
                    name: 'channel',
                    key: 'channel',
                    type: 'textchannel',
                    example: 'f1-discussion',
                    defaultValue: '432208507073331201' // f1-general
                })
            ]
        });
    }

    async run(msg, args) {
        if (!msg.guild.available) {
            return msg.sender.reply('The Guild is currently busy, please post the message manually.', { color: Constants.errorColor });
        }
        const mtvChannel = await msg.guild.channels.resolve(Constants.channels.mtv);
        if (msg.channel.id === args.channel.id) {
            msg.delete();
        } else {
            await msg.sender.reply('Successfully sent the message.');
        }

        if (args.channel === "432208507073331201") {
            const channelToSend = await msg.guild.channels.resolve('432208507073331201');
            return Sender.send(channelToSend, 'Please move to ' + mtvChannel.toString() + '.');
        }

        return Sender.send(args.channel, 'Please move to ' + mtvChannel.toString() + '.');
    }
}

module.exports = new MTV();
