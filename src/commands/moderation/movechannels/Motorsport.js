const patron = require('patron.js');
const Constants = require('../../../utility/Constants.js');
const Sender = require('../../../utility/Sender.js');

class Motorsport extends patron.Command {
  constructor() {
    super({
      names: ['ms', 'motorsport', 'motorsports'],
      groupName: 'moderation',
      description: 'Send the move to #motorsports message to a channel, f1-general by default.',
      args: [
        new patron.Argument({
          name: 'channel',
          key: 'channel',
          type: 'textchannel',
          example: 'f1-discussion',
          defaultValue: '432208507073331201' // f1-general
        })
      ]
    });
  }

  async run(msg, args) {
    if (!msg.guild.available) {
      return msg.sender.reply('The Guild is currently busy, please post the message manually.', { color: Constants.errorColor });
    }

    const motorsportsChannel = await msg.guild.channels.resolve(Constants.channels.motorsports);
    if (msg.channel.id === args.channel.id) {
      msg.delete();
    } else {
      await msg.sender.reply('Successfully sent the message.');
    }

    if (args.channel === "432208507073331201") {
      const channelToSend = await msg.guild.channels.resolve('432208507073331201');
      return Sender.send(channelToSend, 'Please move to ' + motorsportsChannel.toString() + '.');
    }

    return Sender.send(args.channel, 'Please move to ' + motorsportsChannel.toString() + '.');
  }
}

module.exports = new Motorsport();
