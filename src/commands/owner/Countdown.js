const patron = require('patron.js');
const CountdownService = require('../../services/CountdownService.js');
const Discord = require('discord.js');
const db = require('../../database/index.js');

class Countdown extends patron.Command {
    constructor() {
        super({
            names: ['countdown', 'cd', 'startcd', 'startcountdown'],
            groupName: 'owner',
            description: 'Starts the Countdown Bot.'
        });
    }

    async run(msg, args) {
        await msg.delete();
        const embed = new discord.MessageEmbed()
            .setDescription('Starting countdown.')

        const reply = await msg.channel.send({ embed });
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'countdownId': reply.id } });
        await CountdownService.start();
    }
}

module.exports = new Countdown();
