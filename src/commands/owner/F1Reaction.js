const patron = require('patron.js');
const db = require('../../database/index.js');

class F1Reaction extends patron.Command {
    constructor() {
        super({
            names: ['f1reaction'],
            groupName: 'owner',
            description: 'Sends a message that people react to gain / remove roles.'
        });
    }

    async run(msg, args) {
        await msg.delete();
        const reply = await msg.sender.send('By opting in for these channels, you understand that these channels may be highly moderated and that participants are held to a higher standard of civility and respect, especially so in the <#186021984126107649> and <#433229818092322826> channels.\n\nDisciplinary action, such as removal from the channels, mutes, etc. may be imposed upon you at moderators\' discretion if you break any of the channel rules. Read the pinned posts & channel descriptions in the channels, as well as the server rules if in doubt.\n\nReact to this message with :red_circle: for access to the <#186021984126107649> channel.\n' +
            'React to this message with :white_circle: for access to the <#433229818092322826> channel (Only if you have the F4 role).\nReact to this message with <:assetto:706447994471448626> for access to the <#366192692482408450> channel.', { title: 'Opt-in Channels' });
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'f1OptInMessageId': reply.id } });
        await reply.react('🔴'); // Red Circle
        await reply.react('⚪'); // White Circle
        return reply.react('706447994471448626') // Assetto
    }
}

module.exports = new F1Reaction();
