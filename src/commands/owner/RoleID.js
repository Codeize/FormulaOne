const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class RoleID extends patron.Command {
  constructor() {
    super({
      names: ['roleid', 'idrole', 'id'],
      groupName: 'owner',
      description: 'Get the Role ID from a Role.',
      args: [
        new patron.Argument({
          name: 'role',
          key: 'role',
          type: 'string',
          example: 'F4',
          remainder: true
        })
      ]
    });
  }

  async run(msg, args) {
    if (!msg.guild.available) {
      return msg.sender.reply('The Guild is currently busy, please try again later.');
    }
    const role = msg.guild.roles.find('name', args.role);
    if (role === null || role === undefined) {
      return msg.sender.reply('Could not find role ' + args.role.toString() + '.', { color: Constants.errorColor });
    }
    return msg.sender.reply(args.role.toString() + '\'s id is ' + role.id + '.');
  }
}

module.exports = new RoleID();
