class Guild {
  constructor(guildId) {
    this.guildId = guildId;
    this.caseNumber = 1;
    this.lottoEnabled = [];
    this.repeatMessageEnabled = false;
    this.enabledChannels = [];
    this.reactionMessageId = undefined;
    this.f1OptInMessageId = undefined;
    this.countdownId = undefined;
    this.roles = {
      mod: [],
      excluded: [],
      assignable: [],
      banished: undefined,
      muted: undefined
    };
  }
}

module.exports = Guild;
