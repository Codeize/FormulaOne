class Join {
    constructor(userId, guildId) {
        this.userId = userId;
        this.guildId = guildId;
        this.joinedAt = Date.now();
    }
}

module.exports = Join;
