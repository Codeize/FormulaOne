class Pun {
    constructor(userId, guildId, punLength) {
        this.userId = userId;
        this.guildId = guildId;
        this.punLength = punLength;
        this.punishedAt = Date.now();
    }
}

module.exports = Pun;