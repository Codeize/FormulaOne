class User {
  constructor(userId, guildId) {
    this.userId = userId;
    this.guildId = guildId;
    this.balance = 0;
    this.sponsorId = 1;
    this.currentPunishment = 0;
    this.warns = 0;
    this.mutes = 0;
    this.kicks = 0;
    this.bans = 0;
    this.punishments = [];
    this.sponsors = [];
    this.blacklisted = false;
  }
}

module.exports = User;
