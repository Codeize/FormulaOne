const BaseRepository = require('./BaseRepository.js');
const UserQuery = require('../queries/UserQuery.js');
const Join = require('../models/Join.js');

class JoinRepository extends BaseRepository {
    anyJoin(userId, guildId) {
        return this.any(new UserQuery(userId, guildId));
    }

    insertJoin(userId, guildId) {
        return this.insertOne(new Join(userId, guildId));
    }

    findJoin(userId, guildId) {
        return this.find(new UserQuery(userId, guildId));
    }
}

module.exports = JoinRepository;
