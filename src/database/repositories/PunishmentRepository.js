const BaseRepository = require('./BaseRepository.js');
const UserQuery = require('../queries/UserQuery.js');
const Pun = require('../models/Pun.js');

class PunishmentRepository extends BaseRepository {
    anyPun(userId, guildId) {
        return this.any(new UserQuery(userId, guildId));
    }

    insertPun(userId, guildId) {
        return this.insertOne(new Pun(userId, guildId, 2.592e+9)); // 2.592e+9 = 30 days.
    }

    findPun(userId, guildId) {
        return this.find(new UserQuery(userId, guildId));
    }

    deletePun(userId, guildId) {
        return this.deleteOne(new UserQuery(userId, guildId));
    }
}

module.exports = PunishmentRepository;
