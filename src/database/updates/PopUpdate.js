class PopUpdate {
    constructor(property, value) {
        this.$pop = {
            [property]: value
        };
    }
}

module.exports = PopUpdate;
