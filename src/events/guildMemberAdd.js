const ModerationService = require('../services/ModerationService.js');
const Constants = require('../utility/Constants.js');
const db = require('../database');
const client = require('../singletons/client.js');
const Sender = require('../utility/Sender.js');
const StringUtil = require('../utility/StringUtil.js');

client.on('guildMemberAdd', async (member) => {
    const dbGuild = await db.guildRepo.getGuild(member.guild.id);
    // const newMemberRole = await member.guild.roles.fetch('546964268449267722');

    // await member.roles.add(newMemberRole);
    const welcomeChannel = await member.guild.channels.resolve('435404097055883265');
    await Sender.send(welcomeChannel, 'Hello ' + StringUtil.boldify(member.user.tag) + ", welcome to the r/formula1 Discord server! You should be given access to the server in 10 minutes, please take this time to read the <#177387572505346048>.");
    await db.joinRepo.insertJoin(member.id, member.guild.id)

    if (dbGuild.roles.muted !== null && await db.muteRepo.anyMute(member.id, member.guild.id)) {
        const role = await member.guild.roles.fetch(dbGuild.roles.muted);

        if (!(role === undefined || !member.guild.me.permissions.has('MANAGE_ROLES') || role.position >= member.guild.me.roles.highest.position)) {
            return member.roles.add(role);
        }
        const channel = await member.guild.channels.resolve(Constants.modLog);
        const marshalRole = await member.guild.roles.fetch('293845938764644352');
        await ModerationService.tryLogAnything(dbGuild, member.guild, null, member.user.tag + ' is muted and rejoined. I could not give them the Muted role, please do it manually.');
        return channel.send(marshalRole);
    }
});
