const db = require('../database');
const client = require('../singletons/client.js');

client.on('guildMemberUpdate', async (oldMember, newMember) => {
    const dbGuild = await db.guildRepo.getGuild(newMember.guild.id);

    if (oldMember.pending && !newMember.pending) {
        const role = await newMember.guild.roles.fetch('328635502792278017');
        if (role != null) {
            await newMember.roles.add(role);
        }
    }

    if (dbGuild.roles.muted !== null && oldMember.roles.cache.has(dbGuild.roles.muted) && !newMember.roles.cache.has(dbGuild.roles.muted) && await db.muteRepo.anyMute(newMember.id, newMember.guild.id)) {
        return db.muteRepo.deleteMute(newMember.id, newMember.guild.id);
    }
});
