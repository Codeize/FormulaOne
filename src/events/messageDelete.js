const ModerationService = require('../services/ModerationService.js');
const client = require('../singletons/client.js');

client.on('messageDelete', async (message) => {
    if (message.author.bot) {
        return;
    }
    const auditLog = await message.guild.fetchAuditLogs({ type: 'MESSAGE_DELETE' }).then(audit => audit.entries.first());
    if (message.type === 'DEFAULT' && (auditLog.extra.channel.id === message.channel.id) && (auditLog.target.id === message.author.id) && (auditLog.createdTimestamp > (Date.now() - 5000)) && (auditLog.extra.count >= 1)) {
        const mod = auditLog.executor;
        return ModerationService.tryLogAnything(message.dbGuild, message.guild, mod, 'Deleted a message from ' + message.author.toString() + ' (' + message.author.tag + ') in ' + message.channel.toString() + '\n\n**Message**: ' + message.content);
    }
    // Removed logging for messages not deleted by staff as it was spammy (at one point during a GP, 2 logs per second)
});
