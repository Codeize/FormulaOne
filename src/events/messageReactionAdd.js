const Constants = require('../utility/Constants.js');
const client = require('../singletons/client.js');
const db = require('../database/index.js');
const Logger = require('../utility/Logger.js');

client.on('messageReactionAdd', async (messageReaction, user) => {
    if (user.bot) {
        return;
    }
    const msg = messageReaction.message;
    const dbGuild = await db.guildRepo.getGuild(msg.guild.id);
    const member = await msg.guild.members.fetch(user.id);
    if (msg.id === dbGuild.reactionMessageId) {
        const reaction = messageReaction.emoji.name;
        let roleId = null;
        switch (reaction) {
            case 'mercedes':
                roleId = Constants.teamRoles.mercedes;
                break;
            case 'ferrari':
                roleId = Constants.teamRoles.ferrari;
                break;
            case 'redbull':
                roleId = Constants.teamRoles.redbull;
                break;
            case 'mclaren':
                roleId = Constants.teamRoles.mclaren;
                break;
            case 'renault':
                roleId = Constants.teamRoles.renault;
                break;
            case 'alphatauri':
                roleId = Constants.teamRoles.alphatauri;
                break;
            case 'racingpoint':
                roleId = Constants.teamRoles.racingpoint;
                break;
            case 'alfaromeo':
                roleId = Constants.teamRoles.alfaromeo;
                break;
            case 'haas':
                roleId = Constants.teamRoles.haas;
                break;
            case 'williams':
                roleId = Constants.teamRoles.williams;
                break;
        }
        if (roleId === null || roleId === undefined) {
            return Logger.log(`WARNING - Team roleid is null or undefined.`, 'ERROR');
        }
        const role = await msg.guild.roles.fetch(roleId);
        if (member.roles.cache.has(roleId)) {
            return Logger.log(`${user.tag} (${user.id}) - Already has role ` + role.name, 'ERROR');
        }
        await member.roles.add(role);
        return Logger.log(`${user.tag} (${user.id}) - Assigned role ` + role.name, 'DEBUG');
    } else if (msg.id === dbGuild.f1OptInMessageId) {
        const reaction = messageReaction.emoji.name;
        let roleId = null;

        if (!member.roles.cache.has('328635502792278017')) {
            return;
        }

        switch (reaction) {
            case '🔴': // Red Circle
                roleId = Constants.optInRoles.discussion;
                break;
            case '⚪': // White Circle
                roleId = Constants.optInRoles.technical;
                break;
            case 'assetto':
                roleId = Constants.optInRoles.assetto;
                break;
        }
        const f4 = await msg.guild.roles.fetch('313677111695245312');

        if (roleId === Constants.optInRoles.technical && !member.roles.cache.has(f4.id)) {
            return;
        }

        if (roleId === null || roleId === undefined) {
            return Logger.log(`WARNING - Opt in roleid is null or undefined.`, 'ERROR');
        }
        const role = await msg.guild.roles.fetch(roleId);
        if (member.roles.cache.has(roleId)) {
            return Logger.log(`${user.tag} (${user.id}) - Already has role ` + role.name, 'ERROR');
        }
        await member.roles.add(role);
        return Logger.log(`${user.tag} (${user.id}) - Assigned role ` + role.name, 'DEBUG');
    }
});
