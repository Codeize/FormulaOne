const client = require('../singletons/client.js');

const events = {
    MESSAGE_REACTION_ADD: 'messageReactionAdd',
    MESSAGE_REACTION_REMOVE: 'messageReactionRemove',
};

client.on('raw', async event => {
    if (!events.hasOwnProperty(event.t)) return;

    const { d: data } = event;
    const user = await client.users.fetch(data.user_id);
    const channel = await client.channels.fetch(data.channel_id);

    if (channel.messages.cache.has(data.message_id)) return; // Message is already cached so don't run the event twice.

    const message = await channel.messages.fetch(data.message_id);
    const reaction = await message.reactions.resolve(data.emoji.id);

    client.emit(events[event.t], reaction, user);
});
