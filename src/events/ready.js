const Constants = require('../utility/Constants.js');
const Logger = require('../utility/Logger.js');
const client = require('../singletons/client.js');
const type = 'WATCHING';

client.on('ready', async () => {
  (async () => {
    await Logger.log('Formula One has successfully connected.', 'INFO');
    await client.user.setActivity(Constants.game, { type });
    await Logger.log('Set ' + type + ' activity to ' + Constants.game + '.', 'INFO');
  })()
    .catch((err) => Logger.handleError(err));
});
