const patron = require('patron.js');

class BotOwners extends patron.Group {
    constructor() {
        super({
            name: 'administrator',
            description: 'These commands may only be used by Administrators.',
            preconditions: ['administrator']
        });
    }
}

module.exports = new BotOwners();
