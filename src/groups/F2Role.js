const patron = require('patron.js');

class F2Role extends patron.Group {
    constructor() {
        super({
            name: 'f2',
            preconditions: ['f2role']
        });
    }
}

module.exports = new F2Role();
