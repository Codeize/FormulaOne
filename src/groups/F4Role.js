const patron = require('patron.js');

class F4Role extends patron.Group {
    constructor() {
        super({
            name: 'f4',
            preconditions: ['f4role']
        });
    }
}

module.exports = new F4Role();
