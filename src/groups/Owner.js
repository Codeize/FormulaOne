const patron = require('patron.js');

class Owner extends patron.Group {
  constructor() {
    super({
      name: 'owner',
      preconditions: ['owner']
    });
  }
}

module.exports = new Owner();
