const db = require('../database');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Auto Unban', 'DEBUG');

        const bans = await db.banRepo.findMany();

        for (let i = 0; i < bans.length; i++) {
            if (bans[i].bannedAt + bans[i].banLength > Date.now()) {
                continue;
            }

            await db.banRepo.deleteById(bans[i]._id);

            const guild = await client.guilds.fetch(bans[i].guildId);

            if (guild === undefined) {
                continue;
            }

            const user = await client.users.fetch(bans[i].userId);

            if (user === null) {
                continue;
            }

            const dbGuild = await db.guildRepo.getGuild(guild.id);

            await guild.members.unban(user);
            await ModerationService.tryModLog(dbGuild, guild, 'Automatic Unban', Constants.unbanColor, '', null, user);
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.autoUnban);
