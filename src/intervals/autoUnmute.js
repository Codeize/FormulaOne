const db = require('../database');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Auto Unmute', 'DEBUG');

        const mutes = await db.muteRepo.findMany();

        for (let i = 0; i < mutes.length; i++) {
            if (mutes[i].mutedAt + mutes[i].muteLength > Date.now()) {
                continue;
            }

            await db.muteRepo.deleteById(mutes[i]._id);

            const guild = client.guilds.cache.get(mutes[i].guildId);

            if (guild === undefined) {
                continue;
            }

            const member = await guild.members.fetch(mutes[i].userId);

            if (member === null) {
                continue;
            }

            const dbGuild = await db.guildRepo.getGuild(guild.id);
            const role = await guild.roles.fetch(dbGuild.roles.muted);

            if (role === undefined) {
                continue;
            }

            if (!guild.me.permissions.has('MANAGE_ROLES') || role.position >= guild.me.roles.highest.position) {
                continue;
            }

            await member.roles.remove(role);
            await ModerationService.tryModLog(dbGuild, guild, 'Automatic Unmute', Constants.unmuteColor, '', null, member.user);
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.autoUnmute);
