const db = require('../database');
const Constants = require('../utility/Constants.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Auto Join Role', 'DEBUG');

        const joins = await db.joinRepo.findMany();

        for (let i = 0; i < joins.length; i++) {
            if (joins[i].joinedAt + 600000 > Date.now()) {
                continue;
            }

            await db.joinRepo.deleteById(joins[i]._id);

            const guild = client.guilds.cache.get(joins[i].guildId);

            if (guild == null) {
                continue;
            }

            const member = await guild.members.fetch(joins[i].userId);

            if (member === null) {
                continue;
            }

            const role = await guild.roles.fetch('328635502792278017');

            if (role == null) {
                continue;
            }

            if (!guild.me.permissions.has('MANAGE_ROLES') || role.position >= guild.me.roles.highest.position) {
                continue;
            }

            await member.roles.add(role);
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.joinRole);
