const db = require('../database');
const Constants = require('../utility/Constants.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');
const Sender = require('../utility/Sender.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Repeat Message', 'DEBUG');

        const guild = client.guilds.cache.get('177387572505346048');
        const dbGuild = await db.guildRepo.getGuild(guild.id);
        if (dbGuild.repeatMessageEnabled) {
            const f1Channel = await guild.channels.resolve('432208507073331201');
            await Sender.send(f1Channel, "► Read the <#177387572505346048>.\n► We do NOT need your play-by-play commentary.\n► Use <#432208507073331201> for memes/banter.\n► **KEEP THE NEGATIVITY OUT.**  Do not attack a fan base, wish for a driver to crash, etc.");
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.repeatMessage);
