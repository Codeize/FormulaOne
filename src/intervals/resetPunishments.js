const db = require('../database');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Reset Punishments', 'DEBUG');

        const puns = await db.punRepo.findMany();

        for (let i = 0; i < puns.length; i++) {
            if (puns[i].punishedAt + puns[i].punLength > Date.now()) {
                continue;
            }

            await db.punRepo.deleteById(puns[i]._id);

            const guild = await client.guilds.fetch(puns[i].guildId);

            if (guild === undefined) {
                continue;
            }

            const user = await client.users.fetch(puns[i].userId);

            const dbGuild = await db.guildRepo.getGuild(guild.id);

            await db.userRepo.upsertUser(user.id, guild.id, { $set: { currentPunishment: 0 } });
            await ModerationService.tryModLog(dbGuild, guild, 'Automatic Punishment Reset', Constants.unmuteColor, '', null, user);
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.resetPunishments);
