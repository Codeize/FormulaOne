const patron = require('patron.js');

class StatusType extends patron.ArgumentPrecondition {
    constructor() {
        super({
            name: 'statustype'
        });
    }
    async run(command, msg, argument, args, value) {
        const type = value.toUpperCase();
        if (type !== 'PLAYING' && type !== 'WATCHING' && type !== 'LISTENING' && type !== 'STREAMING') {
            return patron.PreconditionResult.fromError(command, 'Invalid type, types:\n\nPLAYING, WATCHING, LISTENING, STREAMING.');
        }

        return patron.PreconditionResult.fromSuccess();
    }
}

module.exports = new StatusType();