const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const ArrayUtil = require('../../utility/ArrayUtil.js');

class Administrator extends patron.Precondition {
    constructor() {
        super({
            name: 'administrator'
        });
    }

    async run(command, msg) {
        if (ModerationService.getPermLevel(msg.dbGuild, msg.member) >= 2) {
            return patron.PreconditionResult.fromSuccess();
        }

        return patron.PreconditionResult.fromError(command, 'You must be a Steward in order to use this command.');
    }
}

module.exports = new Administrator();
