const patron = require('patron.js');

class F4Role extends patron.Precondition {
    constructor() {
        super({
            name: 'f4role'
        });
    }

    async run(command, msg) {
        const f4 = await msg.guild.roles.fetch('313677111695245312');
        if (msg.member.roles.cache.has(f4.id)) {
            return patron.PreconditionResult.fromSuccess();
        }

        return patron.PreconditionResult.fromError(command, 'You must have the F4 role in order to use this command.');
    }
}

module.exports = new F4Role();
