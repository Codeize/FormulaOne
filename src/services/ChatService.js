/* eslint-disable capitalized-comments,max-depth */
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const OneWordService = require('../services/OneWordService.js');
const db = require('../database');
const Random = require('../utility/Random.js');
const USD = require('../utility/USD.js');

class ChatService {
  constructor() {
    this.messages = new Map();
  }

  async applyMoney(msg, sender) {
    if (!msg.member.roles.cache.has('378108874634297355')) {
      return;
    }
    const lastMessage = this.messages.get(msg.author.id);
    const isMessageCooldownOver = lastMessage === undefined || Date.now() - lastMessage > Constants.balance.cooldown;
    const isLongEnough = msg.content.length >= Constants.balance.minLength;

    if (isMessageCooldownOver && isLongEnough) {
      this.messages.set(msg.author.id, Date.now());
      return db.userRepo.modifyBalance(msg.dbGuild, msg.member, Constants.balance.perMessage * 100);
    }
  }

  async checkWord(msg, sender) {
    const enabledChannels = msg.dbGuild.enabledChannels;
    if (enabledChannels.length === 0) {
      return;
    }
    for (let i = 0; i < enabledChannels.length; i++) {
      if (enabledChannels[i].id === msg.channel.id) {
        if (!msg.content.includes(' ')) {
          // Check for URLs.
          if (!msg.content.includes('https://') && !msg.content.includes('http://')) {
            if (ModerationService.getPermLevel(msg.dbGuild, msg.member) < 1) {
              await msg.delete();
              const reply = await msg.sender.reply('You cannot send one word messages.', { color: Constants.errorColor });
              await OneWordService.update(msg, sender);
              return setTimeout(() => reply.delete(), 3000);
            } else {
              return ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Bypassed one word filter in ' + msg.channel.toString() + '\n\n**Message**: ' + msg.content);
            }
          }
        }
      }
    }
  }
}

module.exports = new ChatService();
