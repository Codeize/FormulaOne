const Discord = require('discord.js');
const client = require('../singletons/client.js');
const StringUtil = require('../utility/StringUtil.js');
const db = require('../database/index.js');
const weather = require('openweather-apis');
const credentials = require('../credentials.json');

class CountdownService {

    async start() {
        const races = [
            { "name": "Bahrain Grand Prix", "fp1": new Date(Date.UTC(2019, 2, 29, 11)), "fp2": new Date(Date.UTC(2019, 2, 29, 15)), "fp3": new Date(Date.UTC(2019, 2, 30, 12)), "quali": new Date(Date.UTC(2019, 2, 30, 15)), "race": new Date(Date.UTC(2019, 2, 31, 15, 10)), "flag": ":flag_bh:", "weathercode": 290340 }
        ];

        let first = true;

        weather.setAPPID(credentials.weatherAPIKey);
        weather.setLang('en');


        // Update every 5 seconds.
        let x = setInterval(async function () {
            const now = new Date().getTime();
            const guild = await client.guilds.cache.get('177387572505346048');
            const dbGuild = await db.guildRepo.getGuild('177387572505346048');
            const channel = await guild.channels.resolve('287887687413923842');
            const msg = await channel.messages.fetch(dbGuild.countdownId);

            weather.setCityId(races[0].weathercode);
            await weather.getDescription(async function(err, weatherInfo) {
                let weatherEmote;
                switch(weatherInfo.toLowerCase()) {
                    case "scattered clouds":
                    case "broken clouds":
                    case "overcast clouds":
                    case "few clouds":
                        weatherEmote = ":cloud:";
                        break;
                    case "light rain":
                    case "light intensity drizzle":
                    case "drizzle":
                        weatherEmote = "Light :cloud_rain:";
                        break;
                    case "rain and drizzle":
                        weatherEmote = ":cloud_rain:";
                        break;
                    default:
                        weatherEmote = StringUtil.capitaliseEachWord(weatherInfo);
                }

                let sessionName = 'Null (Tell Fozzie).';
                races[0].fp1 !== undefined ? sessionName = 'Free Practice 1' : null;
                races[0].fp1 === undefined && races[0].fp2 !== undefined ? sessionName = 'Free Practice 2' : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 !== undefined ? sessionName = 'Free Practice 3' : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 === undefined && races[0].quali !== undefined ? sessionName = 'Qualifying' : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 === undefined && races[0].quali === undefined && races[0].race !== undefined ? sessionName = 'Race' : null;

                let session;
                races[0].fp1 !== undefined ? session = races[0].fp1 : null;
                races[0].fp1 === undefined && races[0].fp2 !== undefined ? session = races[0].fp2 : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 !== undefined ? session = races[0].fp3 : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 === undefined && races[0].quali !== undefined ? session = races[0].quali : null;
                races[0].fp1 === undefined && races[0].fp2 === undefined && races[0].fp3 === undefined && races[0].quali === undefined && races[0].race !== undefined ? session = races[0].race : null;
                let distance = session.getTime() - now;
                let duration;

                sessionName === 'Free Practice 1' ? duration = 5400000 : null;
                sessionName === 'Free Practice 2' ? duration = 5400000 : null;
                sessionName === 'Free Practice 3' ? duration = 3600000 : null;
                sessionName === 'Qualifying' ? duration = 3600000 : null;
                sessionName === 'Race' ? duration = 7200000 : null;

                let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                days = days === 0 ? '' : days + ' days, ';
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                hours = hours === 0 ? '' : hours + ' hours, ';
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                minutes = minutes === 0 ? '' : minutes + ' minutes ';
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                const presec = seconds === 0 ? '' : 'and';
                seconds = seconds === 0 ? '.' : ' ' + seconds + ' seconds.';

                let description = `Starting in ${days}${hours}${minutes}${presec}${seconds}`;
                let color = 0xff720e;
                let footer = 'The weather displayed is current weather, not forecasted.';

                if (distance < 0) {
                    if (distance + duration > 0) {
                        description = 'The event is Live!';
                        color = 0x00e828;
                        footer = 'This does not account for delays such as Red Flags etc.';
                        if (first) {
                            const f1Channel = await guild.channels.resolve('186021984126107649');
                            const startEmbed = new discord.MessageEmbed()
                                .setColor(0x00e828)
                                .setDescription((sessionName === 'Race' ? 'The ': '') + sessionName + ' has started!');
                            f1Channel.send({ embed: startEmbed });
                            if (sessionName === 'Qualifying' || sessionName === 'Race') {
                                await f1Channel.edit({ rateLimitPerUser: 15 }, 'Slowmode Auto enabled.');
                                await db.guildRepo.upsertGuild(msg.guild.id, new db.updates.Push('enabledChannels', { id: f1Channel.id }));
                                const enabledEmbed = new discord.MessageEmbed()
                                    .setColor(0x00e828)
                                    .setDescription('Automatically enabled one word filter for ' + f1Channel.toString() + '\nAutomatically enabled Slowmode in ' + f1Channel.toString() + ' for 15 seconds.');
                                f1Channel.send({ embed: enabledEmbed })
                            }
                            first = false;
                        }
                    } else {
                        try {
                            const banishRole = await guild.roles.fetch('411950549064482823');
                            const members = banishRole.members;
                            for (let i = 0; i < members.size; i++) {
                                const member = members[i];
                                await member.roles.remove(banishRole);
                            }
                        } catch(exception) {
                            console.log(exception);
                        }
                        sessionName === 'Free Practice 1' ? delete races[0].fp1 : null;
                        sessionName === 'Free Practice 2' ? delete races[0].fp2 : null;
                        sessionName === 'Free Practice 3' ? delete races[0].fp3 : null;
                        sessionName === 'Qualifying' ? delete races[0].quali : null;
                        sessionName === 'Race' ? delete races[0] : null;
                        first = true;
                    }
                }

                const embed = new discord.MessageEmbed()
                    .setTitle(races[0].flag + ' ' + races[0].name + ' - ' + sessionName + ' - ' + weatherEmote)
                    .setColor(color)
                    .setDescription(description);
                footer !== '' ? embed.setFooter(footer) : null;

                const edited = await msg.edit({ embed });
                await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'countdownId': edited.id } });
            });

        }, 5000);
    }
}

module.exports = new CountdownService();
