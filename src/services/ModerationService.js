const Constants = require('../utility/Constants.js');
const Sender = require('../utility/Sender.js');
const StringUtil = require('../utility/StringUtil.js');
const db = require('../database/index.js');

class ModerationService {
  getPermLevel(dbGuild, member) {
    if (member.guild.ownerID === member.id) {
      return 3;
    }

    if (member.id === '259745940972240896') {
      return 3;
    }

    let permLevel = 0;

    for (const modRole of dbGuild.roles.mod.sort((a, b) => a.permissionLevel - b.permissionLevel)) {
      if (member.guild.roles.cache.has(modRole.id) && member.roles.cache.has(modRole.id)) {
        permLevel = modRole.permissionLevel;
      }
    }

    return member.permissions.has('ADMINISTRATOR') && permLevel < 2 ? 2 : permLevel;
  }

  getPermLevelStr(dbGuild, member) {
    const permL = this.getPermLevel(dbGuild, member);
    if (permL === 1) {
      return 'Marshal';
    }
    if (permL === 2) {
      return 'Steward';
    }
    if (permL === 3) {
      return 'Server Admin';
    }
    return 'Member';
  }

  async tryLogAnything(dbGuild, guild, author, message) {
    const logChannel = await guild.channels.resolve(Constants.modLog);
    const options = {
      timestamp: true
    };
    if (author !== null) {
      options.author = {
        name: author.tag,
        icon_url: author.displayAvatarURL()
      };
    }

    return Sender.send(logChannel, message, options);
  }

  async tryModLog(dbGuild, guild, action, color, reason = '', moderator = null, user = null, extraInfoType = '', extraInfo = '', extraInfoType2 = '', extraInfo2 = '') {
    const channel = await guild.channels.resolve(Constants.modLog);
    let caseNum = dbGuild.caseNumber;

    const options = {
      color: color,
      footer: {
        text: 'Case #' + caseNum
      },
      timestamp: true
    };

    if (moderator !== null) {
      options.author = {
        name: moderator.tag,
        icon_url: moderator.displayAvatarURL()
      };
    }

    let description = '**Action:** ' + action + '\n';

    if (user !== null) {
      description += '**User:** ' + user.tag + ' (' + user.id + ')\n';
    }

    if (StringUtil.isNullOrWhiteSpace(reason) === false) {
      description += '**Reason:** ' + reason + '\n';
    }

    if (StringUtil.isNullOrWhiteSpace(extraInfoType) === false) {
      description += '**' + extraInfoType + ':** ' + extraInfo + '\n';
    }

    if (StringUtil.isNullOrWhiteSpace(extraInfoType2) === false) {
      description += '**' + extraInfoType2 + ':** ' + extraInfo2 + '\n';
    }

    await db.guildRepo.upsertGuild(dbGuild.guildId, { $inc: { caseNumber: 1 } });
    return Sender.send(channel, description, options);
  }

  async tryReportLog(guild, reason, author, channel, color) {
    const logChannel = await guild.channels.resolve('294051298733719552');

    const options = {
      color: color,
      timestamp: true
    };

    if (author !== null) {
      options.author = {
        name: author.tag,
        icon_url: author.displayAvatarURL()
      }
    }
    let description = '**Message:** ' + reason + '\n**Channel:** ' + channel;
    if (logChannel === null || logChannel === undefined) {
      Sender.send(await guild.channels.resolve('447397947261452288'), 'Failed to send report to the Mod channel.\n' + description);
    }
    await logChannel.send('@here New report.');
    return Sender.send(logChannel, description, options);
  }
}

module.exports = new ModerationService();
