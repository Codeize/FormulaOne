const Constants = require('../utility/Constants.js');
const db = require('../database/index.js');
const ModerationService = require('../services/ModerationService.js');

class OneWordService {
    constructor() {
        this.entries = new Map();
    }

    async update(msg, sender) {
        const entry = this.entries.get(msg.author.id);
        if (entry == null || Date.now() - entry.first > Constants.spam.duration * 1e3) {
            this.entries.set(msg.author.id, {
                count: 1,
                first: Date.now()
            });
        } else {
            entry.count++;

            if (entry.count >= Constants.spam.msgLimit) {
                entry.count = 1;
                const role = await msg.guild.roles.fetch(msg.dbGuild.roles.muted);
                if (msg.member.roles.cache.has(role)) { // Doesn't get muted twice.
                    return;
                }
                await msg.member.roles.add(role);
                await db.muteRepo.insertMute(msg.author.id, msg.guild.id, 300000);
                await db.userRepo.upsertUser(msg.author.id, msg.guild.id, { $inc: { mutes: 1 } });
                await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Automatic Mute', Constants.muteColor, 'Continued use of one word messages.', null, msg.author, 'Length', '5 minutes.', 'Channel', msg.channel);
                return msg.sender.dm(msg.author, 'You have been automatically muted for 5 minutes for continued one word usage.');
            }
        }
    }
}

module.exports = new OneWordService();
