const discord = require('discord.js');
const Constants = require('../utility/Constants.js');

module.exports = new discord.Client({ messageCacheMaxSize: 100, messageCacheLifetime: 30, messageSweepInterval: 1800, intents: Constants.intents });
