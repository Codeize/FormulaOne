const { TypeReader, TypeReaderResult } = require("patron.js");
const Constants = require('../utility/Constants.js');

class AssignRoleType extends TypeReader {
    constructor() {
        super({ type: 'assignablerole' });
    }

    filterValues(assignable, roles, fn) {
        const results = [];

        for (const value of roles.values()) {
            if (fn(value) === true) {
                for (let x = 0; x < roles.length; x++) {
                    for (let i = 0; i < assignable.length; i++) {
                        if (roles[x].id === assignable[i].id) {
                            results.push(value);
                        }
                    }
                }
            }
        }
        return results;
    }

    async read(command, message, argument, args, input) {
        if (Constants.regexes.roleMention.test(input) === true || Constants.regexes.id.test(input) === true) {
            const role = await message.guild.roles.fetch(input.match(Constants.regexes.findId)[0]);

            if (role !== undefined) {
                return TypeReaderResult.fromSuccess(role);
            }

            return TypeReaderResult.fromError(command, Constants.errors.roleNotFound);
        }

        const assignable = message.dbGuild.roles.assignable;
        const lowerInput = input.toLowerCase();
        const matches = this.filterValues(assignable, message.guild.roles, (v) => v.name.toLowerCase().includes(lowerInput));

        if (matches.length === 0) {
            return TypeReaderResult.fromError(command, 'Role not found.');
        }
        if (matches.length >= 3) {
            return TypeReaderResult.fromError(command, 'Multiple matches found, please be more specific.');
        }

        let matchesStr = '';
        for (let i = 0; i < matches.length; i++) {
            matchesStr+= matches[i] + ', ';
        }
        return TypeReaderResult.fromError(command, 'Multiple matches found: ' + matchesStr);
    }
}

module.exports = new AssignRoleType();
