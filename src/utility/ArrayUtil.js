class ArrayUtil {
    async removeDuplicates(a) {
        const len = a.length;
        let seen = {};
        let out = [];
        let j = 0;
        for (var i = 0; i < len; i++) {
            var item = a[i];
            if (seen[item] !== 1) {
                seen[item] = 1;
                out[j++] = item;
            }
        }
        return out;
    }
}

module.exports = new ArrayUtil();