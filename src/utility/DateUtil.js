const NumberUtil = require('./NumberUtil.js');

class DateUtil {
  DateSuffix(date) {
      let j = date % 10,
          k = date % 100;
      if (j === 1 && k !== 11) {
          return "st";
      }
      if (j === 2 && k !== 12) {
          return "nd";
      }
      if (j === 3 && k !== 13) {
          return "rd";
      }
      return "th";
  }

  UTCTime(date) {
    return NumberUtil.pad(date.getUTCHours(), 2) + ':' + NumberUtil.pad(date.getUTCMinutes(), 2) + ':' + NumberUtil.pad(date.getUTCSeconds(), 2);
  }

  UTCDate(date) {
    return date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate();
  }
}

module.exports = new DateUtil();
