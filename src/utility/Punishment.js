const db = require('../database/index.js');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const StringUtil = require('../utility/StringUtil.js');
const Try = require('../utility/Try.js');

class Punishment {

    async increasePunishment(memberId, guildId) {
        const dbUser = await db.userRepo.getUser(memberId, guildId);
        const currentPun = dbUser.currentPunishment;
        if (currentPun === 0) {
            await db.punRepo.insertPun(memberId, guildId);
        }
        return db.userRepo.upsertUser(memberId, guildId, { $inc: { currentPunishment: 1 } });
    }

    async decreasePunishment(memberId, guildId) {
        return db.userRepo.upsertUser(memberId, guildId, { $inc: { currentPunishment: -1 } });
    }

    async runPunishCommand(msg, args) {
        const date = new Date();
        const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
        const currentPun = await dbUser.currentPunishment;
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        const reasonSplit = args.reason.split(" ");
        const lastArg = reasonSplit.pop();
        let delArgs = false;
        let reason;
        if (lastArg.toLowerCase() === "-s") {
            msg.delete();
            delArgs = true;
            reason = reasonSplit.join(" ");
        } else {
            reason = args.reason;
        }
        let reply = null;

        if (currentPun === 0) {
            reply = await msg.sender.send('Successfully warned ' + StringUtil.boldify(args.member.user.tag) + ' \n\nThey have ' + currentPun + ' punishments in the last 30 days.');
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { warns: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '1 (Warning)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await this.increasePunishment(args.member.id, msg.guild.id);
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Warn', Constants.warnColor, reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun);
            await msg.sender.dm(args.member.user, 'A moderator has warned you' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 1st punishment in 30 days. Next punishment is a 1 hour mute.'));
        }
        const role = await msg.guild.roles.fetch(msg.dbGuild.roles.muted);
        if (currentPun === 1) {
            if (args.member.roles.cache.has(msg.dbGuild.roles.muted)) {
                await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
            }
            if (role === undefined) {
                return msg.sender.reply('The muted role is not set, please get an Administrator (or Fozzie) to set it using $setmutedrole <Role>', { color: Constants.errorColor });
            }
            reply = await msg.sender.send('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for 1 hour.\n\nThey have ' + currentPun + ' punishment in the last 30 days.');
            await args.member.roles.add(role);
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.muteRepo.insertMute(args.member.id, msg.guild.id, 3600000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '2 (1 hour mute)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '1 hour Mute', Constants.muteColor, reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun);
            await msg.sender.dm(args.member.user, 'A moderator has muted you for 1 hour' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 2nd punishment in 30 days. Next punishment is a 24 hour mute.'));
        }
        if (currentPun === 2) {
            if (args.member.roles.cache.has(msg.dbGuild.roles.muted)) {
                await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
            }
            if (role === undefined) {
                return msg.sender.reply('The muted role is not set, please get an Administrator (or Fozzie) to set it using $setmutedrole <Role>', { color: Constants.errorColor });
            }
            reply = await msg.sender.send('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for 24 hours.\n\nThey have ' + currentPun + ' punishments in the last 30 days.');
            await args.member.roles.add(role);
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.muteRepo.insertMute(args.member.id, msg.guild.id, 86400000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '3 (24 hours mute)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '24 hour Mute', Constants.muteColor, reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun);
            await msg.sender.dm(args.member.user, 'A moderator has muted you for 24 hours' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 3rd punishment in 30 days. Next punishment is a 10 day ban.'));
        }
        if (currentPun === 3) {
            await Try(msg.sender.dm(args.member.user, 'A moderator has banned you for 10 days' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 4th punishment in 30 days. Next punishment is a 30 day ban.')));
            await msg.guild.members.ban(args.member.user);
            reply = await msg.sender.send('Successfully banned ' + StringUtil.boldify(args.member.user.tag) + ' for 10 days.\n\nThey have ' + currentPun + ' punishments in the last 30 days.');
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.banRepo.insertBan(args.member.id, msg.guild.id, 864000000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { bans: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '4 (10 day ban)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '10 day Ban', Constants.banColor, reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun);
        }
        if (currentPun === 4) {
            await Try(msg.sender.dm(args.member.user, 'A moderator has banned you for 30 days' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 4th punishment in 30 days. Next punishment is at the Moderator\'s discretion.')));
            await msg.guild.members.ban(args.member.user);
            reply = await msg.sender.send('Successfully banned ' + StringUtil.boldify(args.member.user.tag) + ' for 30 days.\n\nThey have ' + currentPun + ' punishments in the last 30 days.');
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.banRepo.insertBan(args.member.id, msg.guild.id, 2.592e+9);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { bans: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '5 (30 day ban)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '30 day Ban', Constants.banColor, reason, msg.author, args.member.user, 'Punishments in last 30 days', currentPun);
        }
        if (reply !== null) {
            if (delArgs) {
                setTimeout(() => reply.delete(), 4000);
            }
        }
        if (currentPun > 4) {
            return msg.sender.reply(StringUtil.boldify(args.member.user.tag) + ' has exceeded 5 punishments in the last 30 days, escalate their punishment manually.', { color: Constants.errorColor });
        }
    }
}

module.exports = new Punishment();
