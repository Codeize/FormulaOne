
class SponsorUtil {
    checkTeams(simulatorName, teamName) {
        switch (simulatorName) {
            case 'fozzie':
                switch (teamName) {
                    case 'benetton':
                    case 'tyrrell':
                    case 'williams':
                    case 'mclaren':
                    case 'footwork':
                    case 'simtek':
                    case 'jordan':
                    case 'pacific':
                    case 'forti':
                    case 'minardi':
                    case 'ligier':
                    case 'ferrari':
                    case 'sauber':
                        return true;
                    default:
                        return false;
                }
        }
    }
}

module.exports = new SponsorUtil();
